
var Enum = require('tui-code-snippet').Enum;
var CONST = {};
CONST.FILTER_STATE = new Enum('ALL', 'COMPLETE', 'UNCOMPLETE');
CONST.ENTER_KEYCODE = 13;
CONST.DOM_ELEMENTS = [
    'input',
    'list',
    'uncompleteCount',
    'completeCount',
    'removeTodoButton',
    'filterButton'
];

module.exports = CONST;
